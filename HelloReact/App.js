import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AsyncStorageScreen from './src/AsyncStorageScreen';
import HookScreen from './src/HookScreen';
import RegisterAsync from './src/RegisterScreen'


function HomeScreen() {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
        </View>
    );
}

const Stack = createNativeStackNavigator();

function App() {// shitf + alt + mũi tên xuống : nhân đôi dòng
    return (
        <NavigationContainer>
            <Stack.Navigator> 
                {/* <Stack.Screen name="HookScreen" component={HookScreen} /> */}
                <Stack.Screen name="AsyncStorage" component={AsyncStorageScreen} />
                {/* <Stack.Screen name="Register" component={RegisterSreen} /> */}
                {/* <Stack.Screen name="Home" component={HomeScreen} /> */}
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
