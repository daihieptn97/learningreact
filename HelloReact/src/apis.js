export const postApi = async (link, param) => {
    let formData = new FormData();
    for (const [key, value] of Object.entries(param)) {
        formData.append(key, value);
    }
    return await fetch(link, {
        method: "POST",
        body: formData,
    })
        .then(response => {
            // console.log('response', JSON.stringify(response))
            return response.json();
        })
        .then(json => {
            if (json.status) {
                return json;
            }
            throw json.message;
        })
        .catch(err => {
            __DEV__ && console.log(err);
            if (err.message) {
                throw err.message;
            }
            throw err;
        });
};


export class Apis {
    static baseUrl = "http://localhost/rnApi/";
    static search = Apis.baseUrl + "?func=search";
}
