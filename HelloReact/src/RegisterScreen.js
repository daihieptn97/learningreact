import React, {useEffect, useState} from 'react';
import {Button, View, Text, TextInput, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const STORE_KEY = '@Store_register';

function RegisterScreen() {

    const [data, setData] = useState('');
    const [username = setUsername] = useState("");
    const [password = setPassword ] = useState("");
    const [phonenumber = setPhonenumber] = useState("");

     const storeData = () => {
       // let value = "Hello world 123123";
       let obj = {username: username, password: password, phonenumber: phonenumber};
       try {
         let val = JSON.stringify(obj);
         AsyncStorage.setItem(STORE_KEY, val);
       } catch (e) {
         // saving error
       }
       console.log(
         '🚀 ~ file: AsyncStorageScreen.js ~ line 22 ~ storeData ~ obj',
         obj,
       );
     };

     const removeValue = async () => {
       try {
         await AsyncStorage.removeItem(STORE_KEY);
       } catch (e) {
         // remove error
       }

       console.log('Done.');
     };

     const getData = async () => {
       try {
         const value = await AsyncStorage.getItem(STORE_KEY);
         // Ctrl + alt + L
         if (value !== null) {
           // value previously stored
           setData(value);

           let obj = JSON.parse(value);
           console.log(
             '🚀 ~ file: AsyncStorageScreen.js ~ line 46 ~ getData ~ obj',
             obj,
           );
           setUsername(obj.username);
           setPassword(obj.password);
         } else setData('');
       } catch (e) {
         console.log(e);
         // error reading value
       }
     };

     useEffect(() => {
       getData();
     }, []);

  return (
    <View>
      <Text>Username:</Text>
      <TextInput
        style = {styles.input}
        onChangeText = {t => setUsername(t)}
        value = {username}
      />
      <Text>password</Text>
      <TextInput
        style = {styles.input}
        onChangeText = {t => setPassword(t)}
        value = {password}
      />
      <Text>Phonenumber</Text>
      <TextInput
        style = {styles.input}
        onChangeText = {t => setPhonenumber(t)}
        value = {phonenumber}
      />
      <View>
        <Button title={"Store"} onPress = {storeData} />
        <Button title = {"Get Data"} onPress = {getData} />
        <Button title = {"Remove"} onPress = {removeValue} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
    input:{
        height:50,
        backgroundColor:'white',
        borderWidth: 0.5,
        borderColor:'#bebebe', 
        marginHorizontal:14,
        borderRadius:5,
    }
})

export default React.memo(RegisterScreen)
