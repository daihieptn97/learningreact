import * as React from "react";
import { useEffect, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import FlashMessage, { showMessage } from "react-native-flash-message";
import { postApi } from "./Apis";


const baseUrl = "http://localhost/rnApi/";

function App() {

    const [imgLink, setImageLink] = useState("https://victory8.online/wp-content/uploads/2020/08/zed-lien-minh-huyen-thoai.jpg");

    // useEffect(() => {
    //     // let formData = FormData();
    //     fetch("http://localhost/rnApi/")
    //         .then(response => response.json())
    //         .then(data => console.log(data)).catch(e => {
    //             console.log(e);
    //         },
    //     );
    // }, []);
    //
    // useEffect(() => {
    //     let formData = new FormData();
    //     formData.append("username", "hiep1");
    //     formData.append("password", "abc123");
    //
    //
    //     fetch("http://localhost/rnApi/", {
    //         method: "POST",
    //         headers: {
    //             Accept: "application/json",
    //             "Content-Type": "application/json",
    //         },
    //         body: formData,
    //     })
    //         .then(response => response.json())
    //         .then(data => {
    //             console.log("data o day :", data);
    //         }).catch(e => {
    //             console.log(e);
    //         },
    //     );
    // }, []);


    useEffect(() => {
        let data = {
            "username": "hiep",
            "password": "abc123",
        };

        postApi(baseUrl, data).then(data => {
            console.log(data);
        }).catch(e => {
            console.log(e);
        });
    }, []);

    const onSelectImage = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            // cropping: true,
        }).then(image => {
            console.log(image);
            onUploadImage(image.path, image.mime);
        });
    };

    const onUploadImage = (value, type) => {
        let formData = new FormData();

        let photo = {
            uri: value,
            type: type,
            name: "photo.jpg",
        };
        // console.log("photo", photo);
        formData.append("fileupload", photo);
        // console.log("formData", formData);
        fetch("http://localhost/rnApi/", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: formData,
        })
            .then(response => {
                console.log(response);
                return response.json();
            })
            .then(data => {
                console.log("data: ", data);
                if (data.status) {
                    console.log(baseUrl + data.data);
                    setImageLink(baseUrl + data.data);
                    showMessage({
                        message: "Success Message",
                        description: data.message,
                        type: "success",
                    });
                } else {
                    console.log("upload fail");
                    showMessage({
                        message: "Error Message",
                        description: data.message,
                        type: "danger",
                    });
                }
            }).catch(e => {
                console.log(e);
            },
        );
    };


    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Home Screen</Text>

            <Image source={{ uri: imgLink }} style={{ width: 300, height: 200, margin: 20, resizeMode: "center" }} />
            <TouchableOpacity
                onPress={onSelectImage}
                style={{
                    backgroundColor: "green",
                    borderRadius: 5,
                    width: 200,
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center",
                }}>
                <Text style={{ color: "white" }}>Select Image</Text>
            </TouchableOpacity>
            <FlashMessage />
        </View>
    );
}

export default App;
