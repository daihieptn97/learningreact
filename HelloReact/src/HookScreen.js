import React, { useState, useEffect, useRef } from "react";
import { Button, Text, View, } from "react-native";
import FlashMessage from "react-native-flash-message";

let a = 0;
function HookScreen() {

    const [res, setRes] = useState(0);
    const myLocalFlashMessage = useRef(null)
    console.log(`A = ${a}, Data= ${res}`)

    useEffect(() => {
        // chỉ chạy vào đây đúng 1 lần
        console.log("useEffect");
        return () => {
            // chỉ chạy vào đây đúng 1 lần khi componet die
            console.log("return useEffect detail");
        }
    }, []);

    useEffect(() => {
        // data changer
        console.log("useEffect data");
        return () => {
            console.log("return useEffect data");
        }
    }, [res]);

    return <View>
        <Button title="count Data" onPress={() => setRes(res + 1)} />
        <Button
            // ref={inputRef}
            title="count A"
            onPress={() => {
                a++;
                console.log("🚀 ~ a", a)
                // console.log(myLocalFlashMessage.current);
                myLocalFlashMessage.current.showMessage({
                    message: "Hello World",
                    description: "This is our second message",
                    type: "success",
                });
            }} />
        <Text style={{ fontSize: 18, textAlign: "center" }}>data: {res}</Text>
        <Text style={{ fontSize: 20, marginVertical: 15, textAlign: "center", color: "red" }}>a: {a}</Text>
        
        <FlashMessage position="top" ref={myLocalFlashMessage} />
    </View>;
}

export default React.memo(HookScreen);
