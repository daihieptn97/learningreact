import React, { useEffect, useState } from "react";
import { Button, View, Text, TextInput, StyleSheet } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';

const STORE_KEY = "@storage_user";

function AsyncStorageScreen() {

    const [data, setData] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const storeData = () => {
        // let value = "Hello world 123123";
        let obj = { username: username, password: password }
        try {
            let val = JSON.stringify(obj);
            AsyncStorage.setItem(STORE_KEY, val)
        } catch (e) {
            // saving error
        }
        console.log("🚀 ~ file: AsyncStorageScreen.js ~ line 22 ~ storeData ~ obj", obj)
    }

    const removeValue = async () => {
        try {
            await AsyncStorage.removeItem(STORE_KEY)
        } catch (e) {
            // remove error
        }

        console.log('Done.')
    }

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem(STORE_KEY)
            // Ctrl + alt + L
            if (value !== null) {
                // value previously stored
                setData(value);

                let obj = JSON.parse(value);
                console.log("🚀 ~ file: AsyncStorageScreen.js ~ line 46 ~ getData ~ obj", obj)
                setUsername(obj.username);
                setPassword(obj.password);
            } else setData("")
        } catch (e) {
            console.log(e);
            // error reading value
        }
    }

    useEffect(() => {
        getData();
    }, [])

    // Alt + Shitf + F 
    return <View style={{ flex: 1 }}>
        <Text style={styles.title}>Username:</Text>
        <TextInput
            style={styles.input}
            onChangeText={t => setUsername(t)}
            value={username}
        />
        <Text style={styles.title}>Password: </Text>
        <TextInput
            style={styles.input}
            value={password}
            onChangeText={t => setPassword(t)}
        />

        <Button title={"Store data"} onPress={storeData} />
        <Button title={"Get data"} onPress={getData} />
        <Button title={"Remove data"} onPress={removeValue} />
        <Text style={{ textAlign: "center", fontSize: 16, marginVertical: 12 }}>{data}</Text>
    </View>;
}

const styles = StyleSheet.create({
    title: {
        marginHorizontal: 14,
        marginTop: 12,
        marginBottom: 5

    },
    input: {
        // width: "100%",
        height: 50,
        backgroundColor: "white",
        borderWidth: 0.5,
        borderColor: "#bebebe",
        borderRadius: 5,
        marginHorizontal: 14,

        padding: 14
    }
})

export default React.memo(AsyncStorageScreen);
